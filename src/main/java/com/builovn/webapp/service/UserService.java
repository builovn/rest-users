package com.builovn.webapp.service;

import com.builovn.webapp.domain.entity.*;
import com.builovn.webapp.exception.DuplicateEntityException;
import com.builovn.webapp.exception.NotFoundException;
import com.builovn.webapp.repository.UserRepository;
import com.builovn.webapp.util.EntityUtils;
import org.hibernate.Hibernate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<User> findAll(){
        ArrayList<User> result = new ArrayList<User>();
        userRepository.findAll().forEach(result::add);
        return result;
    }

    public User findById(String id){
        return userRepository.findById(id).orElseThrow(NotFoundException::new);
    }

    public User findByLogin(String login){
        return userRepository.findUserByLogin(login).orElseThrow(NotFoundException::new);
    }

    public User createUser(User user){
        return userRepository.save(user);
    }

    public User updateUser(String id, User user){
        User userUpdate = findById(id);
        BeanUtils.copyProperties(user, userUpdate, "id");
        return userRepository.save(userUpdate);
    }

    public User patchUser(String id, User user){
        User userToPatch = findById(id);
        user.setId(null);
        EntityUtils.patchEntity(Hibernate.unproxy(userToPatch), Hibernate.unproxy(user));
        try {
            return userRepository.save(userToPatch);
        } catch (InvalidDataAccessApiUsageException exception){
            throw new DuplicateEntityException(
                    "The user " + id + " has duplicate pair of department-role or course-speciality", 409);
        }
    }

    public void deleteUserById(String id){
        User user = findById(id);
        userRepository.delete(user);
    }

    public Group addGroup(String userId, Group group){
        User user = findById(userId);
        UserGroup userGroup = new UserGroup(user, group);
        user.getUserGroups().add(userGroup);
        try {
            userRepository.save(user);
        } catch (InvalidDataAccessApiUsageException exception){
            throw new DuplicateEntityException(
                    "The user " + userId + " is already in the group " + group.getId(), 409);
        }
        return group;
    }

    public void deleteGroup(String userId, Long groupId){
        User user = findById(userId);
        user.getUserGroups().removeIf(element -> element.getGroup().getId().equals(groupId));
        userRepository.save(user);
    }

    public List<Group> findAllUserGroupByUserId(String userId){
        User user = findById(userId);
        return user.getUserGroups().stream()
                .map(element -> element.getGroup())
                .collect(Collectors.toList());
    }

}
