package com.builovn.webapp.service;

import com.builovn.webapp.domain.entity.Group;
import com.builovn.webapp.domain.entity.User;
import com.builovn.webapp.domain.entity.UserGroup;
import com.builovn.webapp.exception.DuplicateEntityException;
import com.builovn.webapp.exception.NotFoundException;
import com.builovn.webapp.repository.GroupRepository;
import com.builovn.webapp.util.EntityUtils;
import org.hibernate.Hibernate;
import org.springframework.beans.BeanUtils;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class GroupService {
    private final GroupRepository groupRepository;

    public GroupService(GroupRepository groupRepository){
        this.groupRepository = groupRepository;
    }

    public List<Group> findAll(){
        ArrayList<Group> result = new ArrayList<Group>();
        groupRepository.findAll().forEach(result::add);
        return result;
    }

    public Group findById(long id){
        return groupRepository.findById(id).orElseThrow(
                () -> new NotFoundException("Group with id " + id + " not found.", 404));
    }

    public Group findByName(String name){
        return groupRepository.findGroupByName(name).orElseThrow();
    }

    public Group createGroup(Group group){
        return groupRepository.save(group);
    }

    public Group updateGroup(Long id, Group group){
        Group groupUpdate = findById(id);
        groupUpdate.setName(group.getName());
        return groupRepository.save(groupUpdate);
    }

    public Group patchGroup(Long id, Group group){
        Group groupToPatch = findById(id);
        group.setId(null);
        EntityUtils.patchEntity(Hibernate.unproxy(groupToPatch), Hibernate.unproxy(group));
        return groupRepository.save(groupToPatch);
    }

    public void deleteGroupById(Long id){
        Group group = findById(id);
        groupRepository.delete(group);
    }

    public User addUser(User user, Long groupId){
        Group group = findById(groupId);
        UserGroup userGroup = new UserGroup(user, group);
        group.getUserGroups().add(userGroup);
        try {
            groupRepository.save(group);
        } catch (InvalidDataAccessApiUsageException exception){
            throw new DuplicateEntityException(
                    "The user " + user.getId() + " is already in the group " + group.getId(), 409);
        }
        return user;
    }

    public void deleteGroup(String userId, Long groupId){
        Group group = findById(groupId);
        group.getUserGroups().removeIf(element -> element.getUser().getId().equals(userId));
        groupRepository.save(group);
    }

    public List<User> findAllUserGroupByGroupId(Long groupId){
        Group group = findById(groupId);
        return group.getUserGroups().stream()
                .map(element -> element.getUser())
                .collect(Collectors.toList());
    }
}
