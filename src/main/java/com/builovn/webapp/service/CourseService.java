package com.builovn.webapp.service;

import com.builovn.webapp.domain.entity.Course;
import com.builovn.webapp.domain.entity.Department;
import com.builovn.webapp.exception.NotFoundException;
import com.builovn.webapp.repository.CourseRepository;
import com.builovn.webapp.repository.DepartmentRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CourseService {

    private final CourseRepository courseRepository;

    public CourseService(CourseRepository courseRepository){
        this.courseRepository = courseRepository;
    }

    public List<Course> findAll(){
        ArrayList<Course> result = new ArrayList<Course>();
        courseRepository.findAll().forEach(result::add);
        return result;
    }

    public Course findById(byte id){
        return courseRepository.findById(id).orElseThrow(
                () -> new NotFoundException("Course with id " + id + " not found.", 404));
    }

    public Course createCourse(Course course){
        return courseRepository.save(course);
    }

    public Course updateCourse(byte id, Course course){
        Course courseUpdate = findById(id);
        courseUpdate.setValue(course.getValue());
        return courseRepository.save(courseUpdate);
    }

    public void deleteCourseById(byte id){
        Course course = findById(id);
        courseRepository.delete(course);
    }
}
