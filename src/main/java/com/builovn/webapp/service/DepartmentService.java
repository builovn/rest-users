package com.builovn.webapp.service;

import com.builovn.webapp.domain.entity.Department;
import com.builovn.webapp.domain.entity.User;
import com.builovn.webapp.exception.NotFoundException;
import com.builovn.webapp.repository.DepartmentRepository;
import com.builovn.webapp.repository.UserRepository;
import com.builovn.webapp.util.EntityUtils;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DepartmentService {
    private final DepartmentRepository departmentRepository;

    public DepartmentService(DepartmentRepository departmentRepository){
        this.departmentRepository = departmentRepository;
    }

    public List<Department> findAll(){
        ArrayList<Department> result = new ArrayList<Department>();
        departmentRepository.findAll().forEach(result::add);
        return result;
    }

    public Department findById(long id){
        return departmentRepository.findById(id).orElseThrow(
                () -> new NotFoundException("Department with id " + id + " not found.", 404));
    }

    public Department findByName(String name){
        return departmentRepository.findDepartmentByName(name).orElseThrow(NotFoundException::new);
    }

    public Department createDepartment(Department department){
        return departmentRepository.save(department);
    }

    public Department updateDepartment(Long id, Department department){
        Department departmentUpdate = findById(id);
        departmentUpdate.setName(department.getName());
        departmentUpdate.setParentId(department.getParentId());
        return departmentRepository.save(departmentUpdate);
    }

    public Department patchDepartment(Long id, Department department){
        Department departmentToPatch = findById(id);
        EntityUtils.patchEntity(Hibernate.unproxy(departmentToPatch), Hibernate.unproxy(department));
        return departmentRepository.save(departmentToPatch);
    }

    public void deleteDepartmentById(Long id){
        Department department = findById(id);
        departmentRepository.delete(department);
    }
}
