package com.builovn.webapp.service;

import com.builovn.webapp.domain.entity.Group;
import com.builovn.webapp.domain.entity.Role;
import com.builovn.webapp.exception.NotFoundException;
import com.builovn.webapp.repository.RoleRepository;
import com.builovn.webapp.util.EntityUtils;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RoleService {

    private final RoleRepository roleRepository;

    public RoleService(RoleRepository roleRepository){
        this.roleRepository = roleRepository;
    }

    public List<Role> findAll(){
        ArrayList<Role> result = new ArrayList<Role>();
        roleRepository.findAll().forEach(result::add);
        return result;
    }

    public Role findById(long id){
        return roleRepository.findById(id).orElseThrow(
                () -> new NotFoundException("Role with id " + id + " not found.", 404));
    }

    public Role findByName(String name){
        return roleRepository.findRoleByName(name).orElseThrow(NotFoundException::new);
    }

    public Role createRole(Role role){
        return roleRepository.save(role);
    }

    public Role updateRole(long id, Role role){
        Role userUpdate = findById(id);
        role.setName(role.getName());
        return roleRepository.save(userUpdate);
    }

    public Role patchRole(Long id, Role role){
        Role roleToPatch = findById(id);
        role.setId(null);
        EntityUtils.patchEntity(Hibernate.unproxy(roleToPatch), Hibernate.unproxy(role));
        return roleRepository.save(roleToPatch);
    }

    public void deleteRoleByName(String name){
        Role role = findByName(name);
        roleRepository.delete(role);
    }

    public void deleteRoleById(Long id){
        Role role = findById(id);
        roleRepository.delete(role);
    }
}
