package com.builovn.webapp.service;

import com.builovn.webapp.domain.dto.keycloak.CreateKeycloakUserDto;
import com.builovn.webapp.domain.dto.keycloak.UserWithAttributesDto;
import org.keycloak.adapters.authorization.util.JsonUtils;
import org.keycloak.representations.AccessToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.*;

@Service
public class KeycloakService {
    private final WebClient webClient;
    @Value("${keycloak.realm}")
    private String REALM;

    @Autowired
    public KeycloakService(@Qualifier("Keycloak") WebClient webClient){
        this.webClient = webClient;
    }

    public String createKeycloakUser(CreateKeycloakUserDto user, String accessToken){
        HttpHeaders result = this.webClient.post()
                .uri("/admin/realms/" + REALM + "/users/")
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + accessToken)
                .body(BodyInserters.fromValue(user))
                .exchange()
                .map(response -> response.headers().asHttpHeaders())
                .block();

        String locationHeader = result.getFirst("Location");
        return locationHeader.substring(locationHeader.lastIndexOf('/') + 1);
    }

    public HttpStatus deleteKeycloakUser(String userId, String accessToken){
        HttpStatus statusCode = this.webClient.delete()
                .uri("/admin/realms/" + REALM + "/users/" + userId)
                .header("Authorization", "Bearer " + accessToken)
                .exchange()
                .map(ClientResponse::statusCode)
                .block();

        return statusCode;
    }

    private void setAllUserCustomAttributes(String userId, String accessToken, Map<String, List<String>> userCustomAttributes){
        Map<String, Map<String, List<String>>> attributesWrapper = new HashMap<>();
        attributesWrapper.put("attributes", userCustomAttributes);
        this.webClient.put()
                .uri("/admin/realms/" + REALM + "/users/" + userId)
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + accessToken.toString())
                .body(BodyInserters.fromValue(attributesWrapper))
                .exchange()
                .map(response -> response.headers().asHttpHeaders())
                .block();
    }

    public Map<String, List<String>> getAllUserAttributes(String userId, String accessToken){
        UserWithAttributesDto response = webClient.get()
                .uri("/admin/realms/" + REALM + "/users/" + userId)
                .header("Authorization", "Bearer " + accessToken)
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(UserWithAttributesDto.class)
                .block();
        if(response.getAttributes() == null)
            response.setAttributes(new HashMap<String, List<String>>());
        return response.getAttributes();
    }

    public void setUserCustomAttribute(
            String userId,
            String accessToken,
            String attributeKey,
            List<String> attributeValue){

        Map<String, List<String>> userAttributes = getAllUserAttributes(userId, accessToken);
        userAttributes.put(attributeKey, attributeValue);
        setAllUserCustomAttributes(userId, accessToken, userAttributes);
    }
}
