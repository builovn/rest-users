package com.builovn.webapp.service;

import com.builovn.webapp.domain.entity.Role;
import com.builovn.webapp.domain.entity.Speciality;
import com.builovn.webapp.exception.NotFoundException;
import com.builovn.webapp.repository.SpecialityRepository;
import com.builovn.webapp.repository.SpecialityRepository;
import com.builovn.webapp.util.EntityUtils;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SpecialityService {
    private final SpecialityRepository specialityRepository;

    public SpecialityService(SpecialityRepository specialityRepository){
        this.specialityRepository = specialityRepository;
    }

    public List<Speciality> findAll(){
        ArrayList<Speciality> result = new ArrayList<Speciality>();
        specialityRepository.findAll().forEach(result::add);
        return result;
    }

    public List<Speciality> findAllByDepartmentId(Long id){
        ArrayList<Speciality> result = new ArrayList<Speciality>();
        specialityRepository.findAllByDepartmentId(id).ifPresentOrElse(list -> list.forEach(result::add),
                        () -> new NotFoundException("No specialities of department " + id + " found.", 404));
        return result;
    }

    public Speciality findOneByIdAndDepartmentId(Long id, Long departmentId){
        return specialityRepository.findByIdAndDepartmentId(id, departmentId).orElseThrow(
                () -> new NotFoundException("No speciality with id " + id + "and department id "
                        + departmentId + " found.", 404));
    }

    public Speciality findById(long id){
        return specialityRepository.findById(id).orElseThrow(
                () -> new NotFoundException("Speciality with id " + id + " not found.", 404));
    }

    public Speciality createSpeciality(Speciality speciality){
        return specialityRepository.save(speciality);
    }

    public Speciality updateSpeciality(long id, Speciality speciality){
        Speciality specialityUpdate = findById(id);
        specialityUpdate.setName(speciality.getName());
        return specialityRepository.save(specialityUpdate);
    }

    public Speciality updateSpecialityOfDepartment(Long id, Speciality speciality, Long departmentId){
        Speciality specialityUpdate = findOneByIdAndDepartmentId(id, departmentId);
        specialityUpdate.setName(speciality.getName());
        return specialityRepository.save(specialityUpdate);
    }

    public Speciality patchSpecialityOfDepartment(Long id, Speciality speciality, Long departmentId){
        Speciality specialityToPatch = findOneByIdAndDepartmentId(id, departmentId);
        speciality.setId(null);
        EntityUtils.patchEntity(Hibernate.unproxy(specialityToPatch), Hibernate.unproxy(speciality));
        return specialityRepository.save(specialityToPatch);
    }

    public void deleteSpecialityById(Long id){
        Speciality speciality = findById(id);
        specialityRepository.delete(speciality);
    }

    public void deleteSpecialityOfDepartmentById(Long id, Long departmentId){
        Speciality speciality = findOneByIdAndDepartmentId(id, departmentId);
        specialityRepository.delete(speciality);
    }
}
