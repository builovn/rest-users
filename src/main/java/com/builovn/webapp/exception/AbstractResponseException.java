package com.builovn.webapp.exception;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public abstract class AbstractResponseException extends RuntimeException{

    private int errorCode;
    private String errorMessage;

    public AbstractResponseException(){
        errorMessage = "Missing error message";
    };

    public AbstractResponseException(Throwable throwable) {
        super(throwable);
    }

    public AbstractResponseException(String msg, Throwable throwable) {
        super(msg, throwable);
    }

    public AbstractResponseException(String msg) {
        super(msg);
    }

    public AbstractResponseException(String message, int errorCode) {
        super();
        this.errorCode = errorCode;
        this.errorMessage = message;
    }
}
