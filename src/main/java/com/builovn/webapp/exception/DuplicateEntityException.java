package com.builovn.webapp.exception;

public class DuplicateEntityException extends AbstractResponseException{

    public DuplicateEntityException(){
        super();
    }

    public DuplicateEntityException(String message, int errorCode){
        super(message, errorCode);
    }
}
