package com.builovn.webapp.exception;

public class NotFoundException extends AbstractResponseException{

    public NotFoundException(){
        super();
    }

    public NotFoundException(String message, int errorCode){
        super(message, errorCode);
    }

}
