package com.builovn.webapp.domain.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name="department")
public class Department {
    @Id
    @GeneratedValue()
    private Long id;

    @Column()
    private String name;

    @ManyToOne()
    @JoinColumn(name="parent_id")
    private Department parentId;

    @OneToMany(targetEntity=Speciality.class, mappedBy="department")
    private List<Speciality> specialities;

    public List<Speciality> getSpecialities() {
        if(specialities != null)
            return specialities;
        return new ArrayList<>();
    }
}
