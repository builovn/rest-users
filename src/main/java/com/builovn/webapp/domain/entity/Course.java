package com.builovn.webapp.domain.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Data
@Entity
@Table(name="course")
public class Course {
    @Id
    private Byte value;
}
