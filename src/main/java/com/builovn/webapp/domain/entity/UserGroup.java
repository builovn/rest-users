package com.builovn.webapp.domain.entity;

import com.builovn.webapp.domain.entity.keys.UserGroupKey;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.*;

@Data
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@Entity
@Table(name="users_groups")
public class UserGroup {

    @EmbeddedId
    UserGroupKey id = new UserGroupKey();

    @ManyToOne()
    @MapsId("userId")
    @JoinColumn(name = "user_id")
    @NonNull
    User user;

    @ManyToOne()
    @MapsId("groupId")
    @JoinColumn(name = "group_id")
    @NonNull
    Group group;

    public UserGroup(User user, Group group){
        this.user = user;
        this.group = group;
        this.id.setUserId(user.getId());
        this.id.setGroupId(group.getId());
    }

    public UserGroupKey getId() {
        return id;
    }

    public void setId(UserGroupKey id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }
}
