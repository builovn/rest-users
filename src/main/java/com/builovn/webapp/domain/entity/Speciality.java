package com.builovn.webapp.domain.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Data
@Entity
@Table(name="speciality")
public class Speciality {
    @Id
    @GeneratedValue()
    private Long id;

    @Column
    private String name;

    @ManyToOne
    @JoinColumn(name="department_id")
    private Department department;
}
