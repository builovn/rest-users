package com.builovn.webapp.domain.entity.keys;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Data
@Embeddable
public class UserRoleDepartmentKey implements Serializable {

    @Column(name="user_id")
    private String userId;

    @Column(name="role_id")
    private Long roleId;

    @Column(name="department_id")
    private Long departmentId;
}
