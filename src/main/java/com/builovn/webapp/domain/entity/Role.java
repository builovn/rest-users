package com.builovn.webapp.domain.entity;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Data
@Entity
@Table(name="role")
public class Role {

    @Id
    @GeneratedValue()
    private Long id;

    @Column()
    @JsonProperty("name")
    @NotBlank(message="Blank name field.")
    private String name;
}
