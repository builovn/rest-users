package com.builovn.webapp.domain.entity.keys;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Data
@Embeddable
public class UserGroupKey implements Serializable {

    @Column(name="user_id")
    private String userId;

    @Column(name="group_id")
    private Long groupId;
}
