package com.builovn.webapp.domain.entity.keys;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Data
@Embeddable
public class UserCourseSpecialityKey implements Serializable {

    @Column(name="user_id")
    String userId;

    @Column(name="course")
    Byte courseValue;

    @Column(name="department_id")
    Long specialityId;
}
