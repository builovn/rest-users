package com.builovn.webapp.domain.entity;

import com.builovn.webapp.domain.entity.keys.UserGroupKey;
import com.builovn.webapp.domain.entity.keys.UserRoleDepartmentKey;
import com.builovn.webapp.domain.entity.keys.UserRoleDepartmentKey;
import lombok.*;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@Table(name="users_roles")
public class UserRoleDepartment {

    @EmbeddedId
    UserRoleDepartmentKey id = new UserRoleDepartmentKey();

    @ManyToOne()
    @MapsId("userId")
    @JoinColumn(name = "user_id")
    @NonNull
    User user;

    @ManyToOne()
    @MapsId("roleId")
    @JoinColumn(name = "role_id")
    @NonNull
    Role role;

    @ManyToOne()
    @MapsId("departmentId")
    @JoinColumn(name = "department_id")
    @NonNull
    Department department;

    public UserRoleDepartment(User user, Role role, Department department){
        this.user = user;
        this.role = role;
        this.department = department;
        this.id.setUserId(user.getId());
        this.id.setRoleId(role.getId());
        this.id.setDepartmentId(department  .getId());
    }
}
