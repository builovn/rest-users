package com.builovn.webapp.domain.entity;

import com.builovn.webapp.domain.entity.keys.UserCourseSpecialityKey;
import lombok.*;

import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor
@Table(name="users_courses_specialities")
public class UserCourseSpeciality {

    @EmbeddedId
    UserCourseSpecialityKey id = new UserCourseSpecialityKey();

    @ManyToOne()
    @MapsId("userId")
    @JoinColumn(name="user_id")
    @NonNull
    User user;

    @ManyToOne()
    @MapsId("courseValue")
    @JoinColumn(name="course")
    @NonNull
    Course course;

    @ManyToOne()
    @MapsId("specialityId")
    @JoinColumn(name="speciality_id")
    @NonNull
    Speciality speciality;

    public UserCourseSpeciality(User user, Course course, Speciality speciality){
        this.user = user;
        this.speciality = speciality;
        this.course = course;
        this.id.setCourseValue(course.getValue());
        this.id.setSpecialityId(speciality.getId());
        this.id.setUserId(user.getId());
    }
}
