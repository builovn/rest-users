package com.builovn.webapp.domain.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name="user_group")
public class Group {
    @Id
    @GeneratedValue()
    private Long id;

    @Column()
    @NotBlank(message="Blank name field.")
    private String name;

    @OneToMany(mappedBy = "group", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<UserGroup> userGroups = new ArrayList<>();
}
