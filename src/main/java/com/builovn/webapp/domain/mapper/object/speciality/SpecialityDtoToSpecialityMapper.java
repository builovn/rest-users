package com.builovn.webapp.domain.mapper.object.speciality;

import com.builovn.webapp.domain.dto.speciality.SpecialityDto;
import com.builovn.webapp.domain.entity.Speciality;
import com.builovn.webapp.domain.mapper.AbstractObjectMapper;
import com.builovn.webapp.domain.mapper.object.department.DepartmentDtoToDepartmentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SpecialityDtoToSpecialityMapper extends AbstractObjectMapper<SpecialityDto, Speciality> {
    private final DepartmentDtoToDepartmentMapper departmentMapper;

    @Autowired
    public SpecialityDtoToSpecialityMapper(DepartmentDtoToDepartmentMapper departmentMapper) {
        this.departmentMapper = departmentMapper;
    }

    @Override
    public Speciality convert(SpecialityDto objectToMap) {
        Speciality speciality = new Speciality();
        speciality.setId(objectToMap.getId());
        speciality.setName(objectToMap.getName());
        speciality.setDepartment(departmentMapper.convert(objectToMap.getDepartmentDto()));
        return speciality;
    }

    @Override
    public Class<SpecialityDto> getInClass() {
        return SpecialityDto.class;
    }

    @Override
    public Class<Speciality> getOutClass() {
        return Speciality.class;
    }
}
