package com.builovn.webapp.domain.mapper.object.user;

import com.builovn.webapp.domain.dto.user.CreateOrUpdateUserDto;
import com.builovn.webapp.domain.entity.User;
import com.builovn.webapp.domain.mapper.AbstractObjectMapper;
import com.builovn.webapp.service.UserService;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class CreateOrUpdateUserDtoToUserMapper extends AbstractObjectMapper<CreateOrUpdateUserDto, User> {

    @Override
    public User convert(CreateOrUpdateUserDto objectToMap) {
        User user = new User();
        user.setId(objectToMap.getId());
        user.setLogin(objectToMap.getLogin());
        user.setBirthday(objectToMap.getBirthday());
        user.setFirstName(objectToMap.getFirstName());
        user.setLastName(objectToMap.getLastName());
        user.setEmail(objectToMap.getEmail());
        return user;
    }

    @Override
    public Class<CreateOrUpdateUserDto> getInClass() {
        return CreateOrUpdateUserDto.class;
    }

    @Override
    public Class<User> getOutClass() {
        return User.class;
    }
}
