package com.builovn.webapp.domain.mapper.object.group;

import com.builovn.webapp.domain.dto.group.GroupDto;
import com.builovn.webapp.domain.entity.Group;
import com.builovn.webapp.domain.mapper.AbstractObjectMapper;
import org.springframework.stereotype.Component;

@Component
public class GroupToGroupDtoMapper extends AbstractObjectMapper<Group, GroupDto> {
    @Override
    public GroupDto convert(Group objectToMap) {
        GroupDto groupDto = new GroupDto();
        groupDto.setId(objectToMap.getId());
        groupDto.setName(objectToMap.getName());
        return groupDto;
    }

    @Override
    public Class<Group> getInClass() {
        return Group.class;
    }

    @Override
    public Class<GroupDto> getOutClass() {
        return GroupDto.class;
    }
}
