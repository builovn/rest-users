package com.builovn.webapp.domain.mapper.object.composite;

import com.builovn.webapp.domain.dto.user.AddCourseSpecialityDto;
import com.builovn.webapp.domain.entity.*;
import com.builovn.webapp.domain.mapper.AbstractObjectMapper;
import com.builovn.webapp.service.CourseService;
import com.builovn.webapp.service.SpecialityService;
import com.builovn.webapp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AddCourseSpecialityDtoToUserCourseSpecialityMapper extends AbstractObjectMapper<AddCourseSpecialityDto, UserCourseSpeciality> {
    private final UserService userService;
    private final CourseService courseService;
    private final SpecialityService specialityService;

    @Autowired
    public AddCourseSpecialityDtoToUserCourseSpecialityMapper(UserService userService, CourseService courseService, SpecialityService specialityService) {
        this.userService = userService;
        this.courseService = courseService;
        this.specialityService = specialityService;
    }

    @Override
    public UserCourseSpeciality convert(AddCourseSpecialityDto objectToMap) {
        User user = userService.findById(objectToMap.getUserId());
        Course course = courseService.findById(objectToMap.getCourseValue());
        Speciality speciality = specialityService.findById(objectToMap.getSpecialityId());
        return new UserCourseSpeciality(user, course, speciality);
    }

    @Override
    public Class<AddCourseSpecialityDto> getInClass() {
        return AddCourseSpecialityDto.class;
    }

    @Override
    public Class<UserCourseSpeciality> getOutClass() {
        return UserCourseSpeciality.class;
    }
}
