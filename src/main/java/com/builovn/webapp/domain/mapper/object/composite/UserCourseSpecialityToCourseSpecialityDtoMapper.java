package com.builovn.webapp.domain.mapper.object.composite;

import com.builovn.webapp.domain.dto.composite.CourseSpecialityDto;
import com.builovn.webapp.domain.dto.course.CourseDto;
import com.builovn.webapp.domain.dto.speciality.SpecialityDto;
import com.builovn.webapp.domain.entity.UserCourseSpeciality;
import com.builovn.webapp.domain.mapper.AbstractObjectMapper;
import com.builovn.webapp.domain.mapper.object.course.CourseToCourseDtoMapper;
import com.builovn.webapp.domain.mapper.object.speciality.SpecialityToSpecialityDtoMapper;
import com.builovn.webapp.service.CourseService;
import com.builovn.webapp.service.SpecialityService;
import org.springframework.stereotype.Component;

@Component
public class UserCourseSpecialityToCourseSpecialityDtoMapper extends AbstractObjectMapper<UserCourseSpeciality, CourseSpecialityDto> {

    @Override
    public CourseSpecialityDto convert(UserCourseSpeciality objectToMap) {
        CourseSpecialityDto courseSpecialityDto = new CourseSpecialityDto();
        courseSpecialityDto.setCourseValue(objectToMap.getCourse().getValue());
        courseSpecialityDto.setSpecialityId(objectToMap.getSpeciality().getId());
        return courseSpecialityDto;
    }

    @Override
    public Class<UserCourseSpeciality> getInClass() {
        return UserCourseSpeciality.class;
    }

    @Override
    public Class<CourseSpecialityDto> getOutClass() {
        return CourseSpecialityDto.class;
    }
}
