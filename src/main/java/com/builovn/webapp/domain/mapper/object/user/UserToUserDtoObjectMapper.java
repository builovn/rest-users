package com.builovn.webapp.domain.mapper.object.user;

import com.builovn.webapp.domain.dto.user.UserDto;
import com.builovn.webapp.domain.entity.User;
import com.builovn.webapp.domain.mapper.AbstractObjectMapper;
import com.builovn.webapp.domain.mapper.object.composite.UserCourseSpecialityToCourseSpecialityDtoMapper;
import com.builovn.webapp.domain.mapper.object.composite.UserRoleDepartmentToRoleDepartmentDtoMapper;
import com.builovn.webapp.domain.mapper.object.group.UserGroupToGroupDtoMapper;
import org.springframework.stereotype.Component;

@Component
public class UserToUserDtoObjectMapper extends AbstractObjectMapper<User, UserDto> {

    UserRoleDepartmentToRoleDepartmentDtoMapper roleDepartmentDtoMapper;
    UserCourseSpecialityToCourseSpecialityDtoMapper courseDepartmentDtoMapper;

    public UserToUserDtoObjectMapper(
            UserRoleDepartmentToRoleDepartmentDtoMapper roleDepartmentDtoMapper,
            UserCourseSpecialityToCourseSpecialityDtoMapper courseDepartmentDtoMapper
    ){
        this.roleDepartmentDtoMapper = roleDepartmentDtoMapper;
        this.courseDepartmentDtoMapper = courseDepartmentDtoMapper;
    }

    @Override
    public UserDto convert(User objectToMap) {
        return new UserDto(
                objectToMap.getId(),
                objectToMap.getFirstName(),
                objectToMap.getLastName(),
                objectToMap.getEmail(),
                objectToMap.getBirthday(),
                objectToMap.getLogin(),
                roleDepartmentDtoMapper.convertList(objectToMap.getUserRoleDepartments()),
                courseDepartmentDtoMapper.convertList(objectToMap.getUserCourseSpecialities())
        );
    }

    @Override
    public Class<User> getInClass() {
        return User.class;
    }

    @Override
    public Class<UserDto> getOutClass() {
        return UserDto.class;
    }
}
