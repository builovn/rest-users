package com.builovn.webapp.domain.mapper.object.group;

import com.builovn.webapp.domain.dto.group.CreateOrUpdateGroupDto;
import com.builovn.webapp.domain.entity.Group;
import com.builovn.webapp.domain.mapper.AbstractObjectMapper;
import org.springframework.stereotype.Component;

@Component
public class CreateOrUpdateGroupDtoToGroupMapper extends AbstractObjectMapper<CreateOrUpdateGroupDto, Group> {
    @Override
    public Group convert(CreateOrUpdateGroupDto objectToMap) {
        Group group = new Group();
        group.setName(objectToMap.getName());
        return group;
    }

    @Override
    public Class<CreateOrUpdateGroupDto> getInClass() {
        return CreateOrUpdateGroupDto.class;
    }

    @Override
    public Class<Group> getOutClass() {
        return Group.class;
    }
}
