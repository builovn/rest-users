package com.builovn.webapp.domain.mapper;

import java.util.Objects;

public class MappingKey{
    private final Class<?> in;
    private final Class<?> out;

    public MappingKey(Class<?> in, Class<?> out) {
        this.in = in;
        this.out = out;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MappingKey that = (MappingKey) o;
        return Objects.equals(in, that.in) &&
                Objects.equals(out, that.out);
    }

    @Override
    public int hashCode() {
        return Objects.hash(in, out);
    }
}
