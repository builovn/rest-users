package com.builovn.webapp.domain.mapper;

import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractObjectMapper<IN, OUT> implements IObjectMapper<IN, OUT> {
    @Override
    public List<OUT> convertList(List<IN> listObjectToMap) {
        return listObjectToMap.stream().map(this::convert).collect(Collectors.toList());
    }
}
