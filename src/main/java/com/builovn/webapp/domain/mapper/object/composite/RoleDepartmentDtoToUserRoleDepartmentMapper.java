package com.builovn.webapp.domain.mapper.object.composite;

import com.builovn.webapp.domain.dto.composite.RoleDepartmentDto;
import com.builovn.webapp.domain.entity.UserRoleDepartment;
import com.builovn.webapp.domain.mapper.AbstractObjectMapper;
import com.builovn.webapp.service.DepartmentService;
import com.builovn.webapp.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RoleDepartmentDtoToUserRoleDepartmentMapper extends AbstractObjectMapper<RoleDepartmentDto, UserRoleDepartment> {

    private final DepartmentService departmentService;
    private final RoleService roleService;

    @Autowired
    public RoleDepartmentDtoToUserRoleDepartmentMapper(
            DepartmentService departmentService,
            RoleService roleService
    ){
        this.departmentService = departmentService;
        this.roleService = roleService;
    }

    @Override
    public UserRoleDepartment convert(RoleDepartmentDto objectToMap) {
        UserRoleDepartment userRoleDepartment = new UserRoleDepartment();
        userRoleDepartment.setDepartment(departmentService.findById(objectToMap.getDepartmentId()));
        userRoleDepartment.setRole(roleService.findById(objectToMap.getRoleId()));
        return userRoleDepartment;
    }

    @Override
    public Class<RoleDepartmentDto> getInClass() {
        return RoleDepartmentDto.class;
    }

    @Override
    public Class<UserRoleDepartment> getOutClass() {
        return UserRoleDepartment.class;
    }
}
