package com.builovn.webapp.domain.mapper;

import java.util.List;

public interface IObjectMapper<IN, OUT> {
    OUT convert(IN objectToMap);
    List<OUT> convertList(List<IN> listObjectToMap);

    Class<IN> getInClass();
    Class<OUT> getOutClass();
}
