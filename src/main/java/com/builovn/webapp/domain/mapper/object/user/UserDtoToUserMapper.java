package com.builovn.webapp.domain.mapper.object.user;

import com.builovn.webapp.domain.dto.user.UserDto;
import com.builovn.webapp.domain.entity.User;
import com.builovn.webapp.domain.mapper.AbstractObjectMapper;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class UserDtoToUserMapper extends AbstractObjectMapper<UserDto, User> {

    @Override
    public User convert(UserDto objectToMap) {
        User user = new User();
        user.setId(objectToMap.getId());
        user.setLogin(objectToMap.getLogin());
        user.setBirthday(objectToMap.getBirthday());
        user.setFirstName(objectToMap.getFirstName());
        user.setLastName(objectToMap.getLastName());
        user.setEmail(objectToMap.getEmail());
        user.setUserRoleDepartments(new ArrayList<>());
        return user;
    }

    @Override
    public Class<UserDto> getInClass() {
        return UserDto.class;
    }

    @Override
    public Class<User> getOutClass() {
        return User.class;
    }
}
