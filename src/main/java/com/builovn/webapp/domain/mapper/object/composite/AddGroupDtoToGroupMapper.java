package com.builovn.webapp.domain.mapper.object.composite;

import com.builovn.webapp.domain.dto.user.AddGroupDto;
import com.builovn.webapp.domain.entity.Group;
import com.builovn.webapp.domain.entity.UserGroup;
import com.builovn.webapp.domain.mapper.AbstractObjectMapper;
import com.builovn.webapp.service.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AddGroupDtoToGroupMapper extends AbstractObjectMapper<AddGroupDto, Group> {

    private final GroupService groupService;

    @Autowired
    public AddGroupDtoToGroupMapper(GroupService groupService){
        this.groupService = groupService;
    }

    @Override
    public Group convert(AddGroupDto objectToMap) {
        return groupService.findById(objectToMap.getGroupId());
    }

    @Override
    public Class<AddGroupDto> getInClass() {
        return AddGroupDto.class;
    }

    @Override
    public Class<Group> getOutClass() {
        return Group.class;
    }
}
