package com.builovn.webapp.domain.mapper.object.user;

import com.builovn.webapp.domain.dto.user.PatchUserDto;
import com.builovn.webapp.domain.entity.User;
import com.builovn.webapp.domain.mapper.AbstractObjectMapper;
import com.builovn.webapp.domain.mapper.object.composite.AddCourseSpecialityDtoToUserCourseSpecialityMapper;
import com.builovn.webapp.domain.mapper.object.composite.AddRoleDepartmentDtoToUserRoleDepartmentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PatchUserDtoToUserMapper extends AbstractObjectMapper<PatchUserDto, User> {
    AddCourseSpecialityDtoToUserCourseSpecialityMapper courseDepartmentDtoMapper;
    AddRoleDepartmentDtoToUserRoleDepartmentMapper roleDepartmentDtoMapper;

    @Autowired
    public PatchUserDtoToUserMapper(
            AddRoleDepartmentDtoToUserRoleDepartmentMapper roleDepartmentDtoMapper,
            AddCourseSpecialityDtoToUserCourseSpecialityMapper courseDepartmentDtoMapper
    ){
        this.roleDepartmentDtoMapper = roleDepartmentDtoMapper;
        this.courseDepartmentDtoMapper = courseDepartmentDtoMapper;
    }

    @Override
    public User convert(PatchUserDto objectToMap) {
        User user = new User();
        user.setFirstName(objectToMap.getFirstName());
        user.setLastName(objectToMap.getLastName());
        user.setBirthday(objectToMap.getBirthday());
        user.setEmail(objectToMap.getEmail());
        user.setLogin(objectToMap.getLogin());
        user.setUserCourseSpecialities(courseDepartmentDtoMapper.convertList(objectToMap.getCourseSpecialityList()));
        user.setUserRoleDepartments(roleDepartmentDtoMapper.convertList(objectToMap.getRoleDepartmentList()));
        return user;
    }

    @Override
    public Class<PatchUserDto> getInClass() {
        return PatchUserDto.class;
    }

    @Override
    public Class<User> getOutClass() {
        return User.class;
    }
}
