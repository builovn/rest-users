package com.builovn.webapp.domain.mapper.object.role;

import com.builovn.webapp.domain.dto.role.RoleDto;
import com.builovn.webapp.domain.entity.Role;
import com.builovn.webapp.domain.mapper.AbstractObjectMapper;
import org.springframework.stereotype.Component;

@Component
public class RoleToRoleDtoMapper extends AbstractObjectMapper<Role, RoleDto> {

    @Override
    public RoleDto convert(Role objectToMap) {
        RoleDto result = new RoleDto();
        result.setId(objectToMap.getId());
        result.setName(objectToMap.getName());
        return result;
    }

    @Override
    public Class<Role> getInClass() {
        return Role.class;
    }

    @Override
    public Class<RoleDto> getOutClass() {
        return RoleDto.class;
    }
}
