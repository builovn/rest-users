package com.builovn.webapp.domain.mapper.object.role;

import com.builovn.webapp.domain.dto.role.CreateOrUpdateRoleDto;
import com.builovn.webapp.domain.dto.role.RoleDto;
import com.builovn.webapp.domain.entity.Role;
import com.builovn.webapp.domain.mapper.AbstractObjectMapper;
import org.springframework.stereotype.Component;

@Component
public class CreateOrUpdateRoleDtoToRoleMapper extends AbstractObjectMapper<CreateOrUpdateRoleDto, Role> {
    @Override
    public Role convert(CreateOrUpdateRoleDto objectToMap) {
        Role role = new Role();
        role.setName(objectToMap.getName());
        return role;
    }

    @Override
    public Class<CreateOrUpdateRoleDto> getInClass() {
        return CreateOrUpdateRoleDto.class;
    }

    @Override
    public Class<Role> getOutClass() {
        return Role.class;
    }
}
