package com.builovn.webapp.domain.mapper.object.composite;

import com.builovn.webapp.domain.dto.user.AddRoleDepartmentDto;
import com.builovn.webapp.domain.entity.UserRoleDepartment;
import com.builovn.webapp.domain.mapper.AbstractObjectMapper;
import org.springframework.stereotype.Component;

@Component
public class UserRoleDepartmentToAddRoleDepartmentDtoMapper
        extends AbstractObjectMapper<UserRoleDepartment, AddRoleDepartmentDto> {

    @Override
    public AddRoleDepartmentDto convert(UserRoleDepartment objectToMap) {
        AddRoleDepartmentDto dto = new AddRoleDepartmentDto();
        dto.setUserId(objectToMap.getUser().getId());
        dto.setDepartmentId(objectToMap.getDepartment().getId());
        dto.setRoleId(objectToMap.getRole().getId());
        return dto;
    }

    @Override
    public Class<UserRoleDepartment> getInClass() {
        return UserRoleDepartment.class;
    }

    @Override
    public Class<AddRoleDepartmentDto> getOutClass() {
        return AddRoleDepartmentDto.class;
    }
}