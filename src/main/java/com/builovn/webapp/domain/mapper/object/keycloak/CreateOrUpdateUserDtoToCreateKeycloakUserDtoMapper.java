package com.builovn.webapp.domain.mapper.object.keycloak;

import com.builovn.webapp.domain.dto.user.CreateOrUpdateUserDto;
import com.builovn.webapp.domain.dto.user.UserDto;
import com.builovn.webapp.domain.dto.keycloak.CreateKeycloakUserDto;
import com.builovn.webapp.domain.dto.keycloak.CredentialsDto;
import com.builovn.webapp.domain.mapper.AbstractObjectMapper;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

@Component
public class CreateOrUpdateUserDtoToCreateKeycloakUserDtoMapper extends AbstractObjectMapper<CreateOrUpdateUserDto, CreateKeycloakUserDto> {
    @Override
    public CreateKeycloakUserDto convert(CreateOrUpdateUserDto objectToMap) {
        CredentialsDto credentials = new CredentialsDto();
        credentials.setType("password");
        credentials.setValue(objectToMap.getLogin());
        credentials.setTemporary(true);

        List<CredentialsDto> credentialsList = Collections.singletonList(credentials);

        CreateKeycloakUserDto createKeycloakUserDto = new CreateKeycloakUserDto();
        createKeycloakUserDto.setLogin(objectToMap.getLogin());
        createKeycloakUserDto.setFirstName(objectToMap.getFirstName());
        createKeycloakUserDto.setLastName(objectToMap.getLastName());
        createKeycloakUserDto.setEmail(objectToMap.getEmail());
        createKeycloakUserDto.setEnabled(true);
        createKeycloakUserDto.setCredentials(credentialsList);

        return createKeycloakUserDto;
    }

    @Override
    public Class<CreateOrUpdateUserDto> getInClass() {
        return CreateOrUpdateUserDto.class;
    }

    @Override
    public Class<CreateKeycloakUserDto> getOutClass() {
        return CreateKeycloakUserDto.class;
    }
}
