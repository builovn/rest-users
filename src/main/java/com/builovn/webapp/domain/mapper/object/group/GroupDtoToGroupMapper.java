package com.builovn.webapp.domain.mapper.object.group;

import com.builovn.webapp.domain.dto.group.GroupDto;
import com.builovn.webapp.domain.entity.Group;
import com.builovn.webapp.domain.mapper.AbstractObjectMapper;
import org.springframework.stereotype.Component;

@Component
public class GroupDtoToGroupMapper extends AbstractObjectMapper<GroupDto, Group> {
    @Override
    public Group convert(GroupDto objectToMap) {
        Group group = new Group();
        group.setName(objectToMap.getName());
        return group;
    }

    @Override
    public Class<GroupDto> getInClass() {
        return GroupDto.class;
    }

    @Override
    public Class<Group> getOutClass() {
        return Group.class;
    }
}
