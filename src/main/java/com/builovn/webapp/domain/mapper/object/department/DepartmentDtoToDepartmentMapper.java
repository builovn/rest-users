package com.builovn.webapp.domain.mapper.object.department;

import com.builovn.webapp.domain.dto.department.DepartmentDto;
import com.builovn.webapp.domain.entity.Department;
import com.builovn.webapp.domain.mapper.AbstractObjectMapper;
import org.springframework.stereotype.Component;

@Component
public class DepartmentDtoToDepartmentMapper extends AbstractObjectMapper<DepartmentDto, Department> {

    @Override
    public Department convert(DepartmentDto objectToMap) {
        Department department = new Department();
        department.setName(objectToMap.getName());
        return department;
    }

    @Override
    public Class<DepartmentDto> getInClass() {
        return DepartmentDto.class;
    }

    @Override
    public Class<Department> getOutClass() {
        return Department.class;
    }
}
