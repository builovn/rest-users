package com.builovn.webapp.domain.mapper.object.composite;

import com.builovn.webapp.domain.dto.composite.RoleDepartmentDto;
import com.builovn.webapp.domain.entity.UserRoleDepartment;
import com.builovn.webapp.domain.mapper.AbstractObjectMapper;
import com.builovn.webapp.domain.mapper.object.department.DepartmentToDepartmentDtoMapper;
import com.builovn.webapp.domain.mapper.object.role.RoleToRoleDtoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class    UserRoleDepartmentToRoleDepartmentDtoMapper extends AbstractObjectMapper<UserRoleDepartment, RoleDepartmentDto> {

    @Override
    public RoleDepartmentDto convert(UserRoleDepartment objectToMap) {
        RoleDepartmentDto result = new RoleDepartmentDto();
        result.setRoleId(objectToMap.getRole().getId());
        result.setDepartmentId(objectToMap.getDepartment().getId());
        return result;
    }

    @Override
    public Class<UserRoleDepartment> getInClass() {
        return UserRoleDepartment.class;
    }

    @Override
    public Class<RoleDepartmentDto> getOutClass() {
        return RoleDepartmentDto.class;
    }
}
