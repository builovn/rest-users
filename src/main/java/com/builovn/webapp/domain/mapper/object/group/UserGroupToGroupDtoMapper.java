package com.builovn.webapp.domain.mapper.object.group;

import com.builovn.webapp.domain.dto.group.GroupDto;
import com.builovn.webapp.domain.entity.Group;
import com.builovn.webapp.domain.entity.UserGroup;
import com.builovn.webapp.domain.mapper.AbstractObjectMapper;
import org.springframework.stereotype.Component;

@Component
public class UserGroupToGroupDtoMapper extends AbstractObjectMapper<UserGroup, GroupDto> {
    @Override
    public GroupDto convert(UserGroup objectToMap) {
        GroupDto groupDto = new GroupDto();
        Group group = objectToMap.getGroup();
        groupDto.setName(group.getName());
        return groupDto;
    }

    @Override
    public Class<UserGroup> getInClass() {
        return UserGroup.class;
    }

    @Override
    public Class<GroupDto> getOutClass() {
        return GroupDto.class;
    }
}
