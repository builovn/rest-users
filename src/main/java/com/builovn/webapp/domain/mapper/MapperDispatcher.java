package com.builovn.webapp.domain.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class MapperDispatcher {
    private final Map<MappingKey, IObjectMapper<?,?>> mappersMap;

    public MapperDispatcher(@Autowired List<IObjectMapper<?,?>> mappers) {
        HashMap<MappingKey, IObjectMapper<?,?>> tmpMappers = new HashMap<>();
        for (var mapper : mappers) {
            tmpMappers.put(new MappingKey(mapper.getInClass(), mapper.getOutClass()), mapper);
        }
        this.mappersMap = Collections.unmodifiableMap(tmpMappers);
    }

    @SuppressWarnings("unchecked")
    public <IN, OUT> OUT convert(IN obj, Class<OUT> destClass) {
        if (obj == null) {
            return null;
        }
        IObjectMapper<IN, OUT> mapper = (IObjectMapper<IN, OUT>)mappersMap.get(new MappingKey(obj.getClass(), destClass));
        if (mapper == null) {
            throw new RuntimeException("Unsupported mapper. " + obj.getClass() + " -> " + destClass);
        }
        return mapper.convert(obj);
    }

    @SuppressWarnings("unchecked")
    public <IN, OUT> List<OUT> convertList(List<IN> objList, Class<OUT> destClass) {
        if (objList.isEmpty()) {
            return Collections.EMPTY_LIST;
        }
        IObjectMapper<IN, OUT> mapper = (IObjectMapper<IN, OUT>)mappersMap.get(new MappingKey(objList.get(0).getClass(), destClass));
        if (mapper == null) {
            throw new RuntimeException("Unsupported mapper. " + objList.getClass() + " -> " + destClass);
        }
        return mapper.convertList(objList);
    }
}
