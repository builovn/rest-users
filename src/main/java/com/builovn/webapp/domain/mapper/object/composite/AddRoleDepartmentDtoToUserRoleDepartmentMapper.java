package com.builovn.webapp.domain.mapper.object.composite;

import com.builovn.webapp.domain.dto.user.AddRoleDepartmentDto;
import com.builovn.webapp.domain.entity.Department;
import com.builovn.webapp.domain.entity.Role;
import com.builovn.webapp.domain.entity.User;
import com.builovn.webapp.domain.entity.UserRoleDepartment;
import com.builovn.webapp.domain.mapper.AbstractObjectMapper;
import com.builovn.webapp.service.DepartmentService;
import com.builovn.webapp.service.RoleService;
import com.builovn.webapp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AddRoleDepartmentDtoToUserRoleDepartmentMapper extends AbstractObjectMapper<AddRoleDepartmentDto, UserRoleDepartment> {

    private final RoleService roleService;
    private final DepartmentService departmentService;
    private final UserService userService;

    @Autowired
    public AddRoleDepartmentDtoToUserRoleDepartmentMapper(RoleService roleService, DepartmentService departmentService, UserService userService){

        this.roleService = roleService;
        this.departmentService = departmentService;
        this.userService = userService;
    }

    @Override
    public UserRoleDepartment convert(AddRoleDepartmentDto objectToMap) {
        Department department = departmentService.findById(objectToMap.getDepartmentId());
        Role role = roleService.findById(objectToMap.getRoleId());
        User user = userService.findById(objectToMap.getUserId());
        return new UserRoleDepartment(user, role, department);
    }

    @Override
    public Class<AddRoleDepartmentDto> getInClass() {
        return AddRoleDepartmentDto.class;
    }

    @Override
    public Class<UserRoleDepartment> getOutClass() {
        return UserRoleDepartment.class;
    }
}
