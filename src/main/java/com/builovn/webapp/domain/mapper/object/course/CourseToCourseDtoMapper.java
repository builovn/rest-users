package com.builovn.webapp.domain.mapper.object.course;

import com.builovn.webapp.domain.dto.course.CourseDto;
import com.builovn.webapp.domain.entity.Course;
import com.builovn.webapp.domain.mapper.AbstractObjectMapper;
import org.springframework.stereotype.Component;

@Component
public class CourseToCourseDtoMapper extends AbstractObjectMapper<Course, CourseDto> {
    @Override
    public CourseDto convert(Course objectToMap) {
        CourseDto courseDto = new CourseDto();
        courseDto.setValue(objectToMap.getValue());
        return courseDto;
    }

    @Override
    public Class<Course> getInClass() {
        return Course.class;
    }

    @Override
    public Class<CourseDto> getOutClass() {
        return CourseDto.class;
    }
}