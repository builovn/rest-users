package com.builovn.webapp.domain.mapper.object.speciality;

import com.builovn.webapp.domain.dto.speciality.SpecialityDto;
import com.builovn.webapp.domain.entity.Speciality;
import com.builovn.webapp.domain.mapper.AbstractObjectMapper;
import com.builovn.webapp.domain.mapper.object.department.DepartmentToDepartmentDtoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SpecialityToSpecialityDtoMapper extends AbstractObjectMapper<Speciality, SpecialityDto> {
    private final DepartmentToDepartmentDtoMapper departmentMapper;

    @Autowired
    public SpecialityToSpecialityDtoMapper(DepartmentToDepartmentDtoMapper departmentMapper) {
        this.departmentMapper = departmentMapper;
    }

    @Override
    public SpecialityDto convert(Speciality objectToMap) {
        SpecialityDto dto = new SpecialityDto();
        dto.setId(objectToMap.getId());
        dto.setName(objectToMap.getName());
        dto.setDepartmentDto(departmentMapper.convert(objectToMap.getDepartment()));
        return dto;
    }

    @Override
    public Class<Speciality> getInClass() {
        return Speciality.class;
    }

    @Override
    public Class<SpecialityDto> getOutClass() {
        return SpecialityDto.class;
    }
}
