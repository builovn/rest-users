package com.builovn.webapp.domain.mapper.object.department;

import com.builovn.webapp.domain.dto.department.CreateOrUpdateDepartmentDto;
import com.builovn.webapp.domain.dto.group.CreateOrUpdateGroupDto;
import com.builovn.webapp.domain.entity.Department;
import com.builovn.webapp.domain.mapper.AbstractObjectMapper;
import org.springframework.stereotype.Component;

@Component
public class CreateOrUpdateDepartmentDtoToDepartmentMapper
        extends AbstractObjectMapper<CreateOrUpdateDepartmentDto, Department> {

    @Override
    public Department convert(CreateOrUpdateDepartmentDto objectToMap) {
        Department department = new Department();
        department.setName(objectToMap.getName());
        return department;
    }

    @Override
    public Class<CreateOrUpdateDepartmentDto> getInClass() {
        return CreateOrUpdateDepartmentDto.class;
    }

    @Override
    public Class<Department> getOutClass() {
        return Department.class;
    }
}
