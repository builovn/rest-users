package com.builovn.webapp.domain.mapper.object.speciality;

import com.builovn.webapp.domain.dto.speciality.CreateOrUpdateSpecialityDto;
import com.builovn.webapp.domain.entity.Speciality;
import com.builovn.webapp.domain.mapper.AbstractObjectMapper;
import com.builovn.webapp.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CreateOrUpdateSpecialityDtoToSpecialityMapper extends AbstractObjectMapper<CreateOrUpdateSpecialityDto, Speciality> {
    private final DepartmentService departmentService;

    @Autowired
    public CreateOrUpdateSpecialityDtoToSpecialityMapper(DepartmentService departmentService) {
        this.departmentService = departmentService;
    }

    @Override
    public Speciality convert(CreateOrUpdateSpecialityDto objectToMap) {
        Speciality speciality = new Speciality();
        speciality.setName(objectToMap.getName());
        return speciality;
    }

    @Override
    public Class<CreateOrUpdateSpecialityDto> getInClass() {
        return CreateOrUpdateSpecialityDto.class;
    }

    @Override
    public Class<Speciality> getOutClass() {
        return Speciality.class;
    }
}
