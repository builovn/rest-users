package com.builovn.webapp.domain.mapper.object.department;

import com.builovn.webapp.domain.dto.department.DepartmentDto;
import com.builovn.webapp.domain.entity.Department;
import com.builovn.webapp.domain.mapper.AbstractObjectMapper;
import org.springframework.stereotype.Component;

@Component
public class DepartmentToDepartmentDtoMapper extends AbstractObjectMapper<Department, DepartmentDto> {

    @Override
    public DepartmentDto convert(Department objectToMap) {
        DepartmentDto result = new DepartmentDto();
        result.setId(objectToMap.getId());
        result.setName(objectToMap.getName());
        return result;
    }

    @Override
    public Class<Department> getInClass() {
        return Department.class;
    }

    @Override
    public Class<DepartmentDto> getOutClass() {
        return DepartmentDto.class;
    }
}
