package com.builovn.webapp.domain.mapper.object.course;

import com.builovn.webapp.domain.dto.course.CourseDto;
import com.builovn.webapp.domain.entity.Course;
import com.builovn.webapp.domain.mapper.AbstractObjectMapper;
import org.springframework.stereotype.Component;

@Component
public class CourseDtoToCourseMapper extends AbstractObjectMapper<CourseDto, Course> {
    @Override
    public Course convert(CourseDto objectToMap) {
        Course course = new Course();
        course.setValue(objectToMap.getValue());
        return course;
    }

    @Override
    public Class<CourseDto> getInClass() {
        return CourseDto.class;
    }

    @Override
    public Class<Course> getOutClass() {
        return Course.class;
    }
}
