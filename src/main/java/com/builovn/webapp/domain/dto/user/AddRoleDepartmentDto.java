package com.builovn.webapp.domain.dto.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


@Data
public class AddRoleDepartmentDto {
    @JsonIgnore
    private String userId;

    @JsonProperty("role_id")
    @NotNull(message="Blank role name field.")
    private Long roleId;

    @JsonProperty("department_id")
    @NotNull(message="Blank department name field.")
    private Long departmentId;
}
