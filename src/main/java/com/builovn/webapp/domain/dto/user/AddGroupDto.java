package com.builovn.webapp.domain.dto.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class AddGroupDto {
    @JsonIgnore
    private String userId;

    @JsonProperty("id")
    @NotNull(message = "Blank group field.")
    private Long groupId;
}
