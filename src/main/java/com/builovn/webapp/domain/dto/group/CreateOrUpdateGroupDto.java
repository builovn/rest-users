package com.builovn.webapp.domain.dto.group;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class CreateOrUpdateGroupDto {
    @JsonProperty("name")
    @NotBlank(message="Blank group name field.")
    private String name;
}
