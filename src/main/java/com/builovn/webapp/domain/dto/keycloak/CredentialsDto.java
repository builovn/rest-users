package com.builovn.webapp.domain.dto.keycloak;

import lombok.Data;

@Data
public class CredentialsDto {
    private String type;
    private String value;
    private Boolean temporary;
}
