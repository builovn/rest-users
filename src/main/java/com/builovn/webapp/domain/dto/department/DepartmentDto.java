package com.builovn.webapp.domain.dto.department;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.JoinColumn;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class DepartmentDto {
    @JsonProperty("id")
    @NotNull(message = "Blank department id field.")
    private Long id;

    @JsonProperty("name")
    @NotBlank(message = "Blank department name field.")
    private String name;
}
