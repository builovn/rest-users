package com.builovn.webapp.domain.dto.group;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class GroupDto {
    @JsonProperty("id")
    @NotNull(message="Blank group id field.")
    private Long id;

    @JsonProperty("name")
    @NotBlank(message="Blank group name field.")
    private String name;
}
