package com.builovn.webapp.domain.dto.composite;

import com.builovn.webapp.domain.dto.course.CourseDto;
import com.builovn.webapp.domain.dto.speciality.SpecialityDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class CourseSpecialityDto {
    @JsonProperty("course_value")
    @NotNull(message = "Blank course field.")
    private byte courseValue;

    @JsonProperty("speciality_id")
    @NotNull(message = "Blank speciality field.")
    private long specialityId;
}
