package com.builovn.webapp.domain.dto.role;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class RoleDto {
    @JsonProperty("id")
    @NotNull(message = "Blank role id field.")
    private Long id;

    @JsonProperty("name")
    @NotBlank(message = "Blank role name field.")
    private String name;
}
