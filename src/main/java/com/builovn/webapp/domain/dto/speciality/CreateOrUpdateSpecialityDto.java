package com.builovn.webapp.domain.dto.speciality;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class CreateOrUpdateSpecialityDto {
    @JsonProperty("name")
    @NotBlank(message = "Blank speciality name field.")
    private String name;
}
