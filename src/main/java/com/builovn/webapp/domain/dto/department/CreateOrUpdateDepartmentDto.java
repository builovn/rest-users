package com.builovn.webapp.domain.dto.department;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class CreateOrUpdateDepartmentDto {
    @JsonProperty("name")
    @NotBlank(message = "Blank department name field.")
    private String name;
}
