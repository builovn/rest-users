package com.builovn.webapp.domain.dto.course;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class CourseDto {
    @JsonProperty("value")
    @NotNull(message = "Blank course value field.")
    private Byte value;
}
