package com.builovn.webapp.domain.dto.speciality;

import com.builovn.webapp.domain.dto.department.DepartmentDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class SpecialityDto {
    @JsonProperty("id")
    private Long id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("department")
    private DepartmentDto departmentDto;
}
