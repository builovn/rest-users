package com.builovn.webapp.domain.dto.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class AddCourseSpecialityDto {
    @JsonIgnore
    private String userId;

    @JsonProperty("course_value")
    @NotNull(message = "Blank course field.")
    private Byte courseValue;

    @JsonProperty("speciality_id")
    @NotNull(message = "Blank speciality field.")
    private Long specialityId;
}
