package com.builovn.webapp.domain.dto.user;

import com.builovn.webapp.domain.dto.composite.CourseSpecialityDto;
import com.builovn.webapp.domain.dto.composite.RoleDepartmentDto;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Data
public class PatchUserDto {
    @JsonIgnore
    private String id;

    @JsonProperty("first_name")
    private String firstName;

    @JsonProperty("last_name")
    private String lastName;

    @JsonProperty("email")
    private String email;

    @JsonProperty("birthdate")
    private Date birthday;

    @JsonProperty("login")
    private String login;

    @Valid
    @JsonProperty("course_speciality")
    private List<AddCourseSpecialityDto> courseSpecialityList;

    @Valid
    @JsonProperty("role_department")
    private List<AddRoleDepartmentDto> roleDepartmentList;

    public List<AddRoleDepartmentDto> getRoleDepartmentList() {
        if(roleDepartmentList != null)
            return roleDepartmentList;
        return new ArrayList<>();
    }

    public List<AddCourseSpecialityDto> getCourseSpecialityList() {
        if(courseSpecialityList != null)
            return courseSpecialityList;
        return new ArrayList<>();
    }
}
