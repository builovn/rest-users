package com.builovn.webapp.domain.dto.user;

import com.builovn.webapp.domain.dto.composite.CourseSpecialityDto;
import com.builovn.webapp.domain.dto.group.GroupDto;
import com.builovn.webapp.domain.dto.composite.RoleDepartmentDto;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;

import javax.validation.constraints.NotBlank;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Data
public class UserDto {
    @JsonProperty("id")
    private String id;

    @JsonProperty("first_name")
    @NotBlank(message="Blank first name field.")
    private String firstName;

    @JsonProperty("last_name")
    @NotBlank(message="Blank last name field.")
    private String lastName;

    @JsonProperty("email")
    @NotBlank(message="Blank email field.")
    private String email;

    @JsonProperty("birthdate")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date birthday;

    @JsonProperty("login")
    @NotBlank(message="Blank login field.")
    private String login;

    @Getter(AccessLevel.NONE)
    @JsonProperty("role_department")
    private List<RoleDepartmentDto> roleDepartmentDtoList;

    @Getter(AccessLevel.NONE)
    @JsonProperty("course_speciality")
    private List<CourseSpecialityDto> courseSpecialityDtoList;

    public UserDto(String id, String firstName, String lastName, String email, Date birthday, String login,
                   List<RoleDepartmentDto> roleDepartmentDtoList,
                   List<CourseSpecialityDto> courseSpecialityDtoList) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.birthday = birthday;
        this.login = login;
        this.roleDepartmentDtoList = roleDepartmentDtoList;
        this.courseSpecialityDtoList = courseSpecialityDtoList;
    }

    public List<RoleDepartmentDto> getRoleDepartmentDtoList() {
        if(roleDepartmentDtoList != null)
            return roleDepartmentDtoList;
        return new ArrayList<>();
    }

    public List<CourseSpecialityDto> getCourseSpecialityDtoList() {
        if(courseSpecialityDtoList != null)
            return courseSpecialityDtoList;
        return new ArrayList<>();
    }
}
