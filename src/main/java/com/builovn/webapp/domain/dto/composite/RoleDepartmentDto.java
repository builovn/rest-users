package com.builovn.webapp.domain.dto.composite;

import com.builovn.webapp.domain.dto.department.DepartmentDto;
import com.builovn.webapp.domain.dto.role.RoleDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class RoleDepartmentDto {
    @JsonProperty("role_id")
    @NotNull(message = "Blank role field.")
    private Long roleId;

    @JsonProperty("department_id")
    @NotNull(message = "Blank department field.")
    private Long departmentId;
}
