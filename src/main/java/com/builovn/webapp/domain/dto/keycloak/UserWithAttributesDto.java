package com.builovn.webapp.domain.dto.keycloak;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class UserWithAttributesDto {
    private String id;
    private String username;
    private String firstName;
    private String lastName;
    private String email;
    private Map<String, List<String>> attributes;
}
