package com.builovn.webapp.domain.dto.keycloak;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class CreateKeycloakUserDto {
    @JsonProperty("username")
    private String login;

    @JsonProperty("email")
    private String email;

    @JsonProperty("firstName")
    private String firstName;

    @JsonProperty("lastName")
    private String lastName;

    @JsonProperty("credentials")
    private List<CredentialsDto> credentials;

    @JsonProperty("enabled")
    private Boolean enabled;
}
