package com.builovn.webapp.domain.dto.user;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.sql.Date;

@Data
public class CreateOrUpdateUserDto {
    @JsonIgnore
    private String id;

    @JsonProperty("first_name")
    @NotBlank(message="Blank first name field.")
    private String firstName;

    @JsonProperty("last_name")
    @NotBlank(message="Blank last name field.")
    private String lastName;

    @JsonProperty("email")
    @NotBlank(message="Blank email field.")
    private String email;

    @JsonProperty("birthdate")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date birthday;

    @JsonProperty("login")
    @NotBlank(message="Blank login field.")
    private String login;
}
