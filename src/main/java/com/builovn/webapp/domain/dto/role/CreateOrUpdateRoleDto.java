package com.builovn.webapp.domain.dto.role;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class CreateOrUpdateRoleDto {
    @JsonProperty("name")
    @NotBlank(message = "Blank role name field.")
    private String name;
}
