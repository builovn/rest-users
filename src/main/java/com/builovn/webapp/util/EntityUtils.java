package com.builovn.webapp.util;

import java.lang.reflect.Field;
import java.util.Collection;

public class EntityUtils {

    @SuppressWarnings("unchecked")
    public static <T> void patchEntity(T object, T patchedObject){
        Class objectClass = object.getClass();
        Field[] fields = objectClass.getDeclaredFields();
        for(Field field : fields){
            try {
                field.setAccessible(true);
                Object patchedField = field.get(patchedObject);
                if(patchedField != null){
                    if(Collection.class.isAssignableFrom(field.getType())){
                        Collection objectField = (Collection)field.get(object);
                        objectField.clear();
                        objectField.addAll((Collection) patchedField);
                    }
                    else {
                        field.set(object, field.get(patchedObject));
                    }
                }
            } catch (IllegalAccessException exception){
                throw new RuntimeException("Failed to access field: " + field.getName() + " of class: "
                        + objectClass.getName());
            }
        }
    }
}
