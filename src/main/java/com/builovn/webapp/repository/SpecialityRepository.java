package com.builovn.webapp.repository;

import com.builovn.webapp.domain.entity.Speciality;
import org.hibernate.cfg.JPAIndexHolder;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface SpecialityRepository extends JpaRepository<Speciality, Long> {
    Optional<List<Speciality>> findAllByDepartmentId(Long departmentId);
    Optional<Speciality> findByIdAndDepartmentId(Long id, Long departmentId);
}
