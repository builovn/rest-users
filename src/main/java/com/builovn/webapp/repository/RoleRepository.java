package com.builovn.webapp.repository;

import com.builovn.webapp.domain.entity.Role;
import com.builovn.webapp.domain.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findRoleByName(String name);
}
