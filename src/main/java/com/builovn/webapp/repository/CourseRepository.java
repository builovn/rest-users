package com.builovn.webapp.repository;

import com.builovn.webapp.domain.entity.Course;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CourseRepository extends JpaRepository<Course, Byte> {
    
}
