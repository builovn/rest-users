package com.builovn.webapp.repository;

import com.builovn.webapp.domain.entity.Group;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface GroupRepository extends JpaRepository<Group, Long> {
    public Optional<Group> findGroupByName(String name);
}
