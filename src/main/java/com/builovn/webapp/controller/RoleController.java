package com.builovn.webapp.controller;

import com.builovn.webapp.domain.dto.group.CreateOrUpdateGroupDto;
import com.builovn.webapp.domain.dto.group.GroupDto;
import com.builovn.webapp.domain.dto.role.CreateOrUpdateRoleDto;
import com.builovn.webapp.domain.dto.role.RoleDto;
import com.builovn.webapp.domain.entity.Group;
import com.builovn.webapp.domain.entity.Role;
import com.builovn.webapp.domain.mapper.MapperDispatcher;
import com.builovn.webapp.service.RoleService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/v1/roles")
public class RoleController extends BaseController{
    private final RoleService roleService;
    private final MapperDispatcher mapper;

    public RoleController(RoleService roleService, MapperDispatcher mapper) {
        this.roleService = roleService;
        this.mapper = mapper;
    }

    @GetMapping
    public ResponseEntity<List<RoleDto>> list() {
        List<RoleDto> roleList = mapper.convertList(roleService.findAll(), RoleDto.class);
        return new ResponseEntity<>(roleList, HttpStatus.OK);
    }

    @GetMapping("{id}")
    public ResponseEntity<RoleDto> getOne(@PathVariable Long id){
        RoleDto responseRole = mapper.convert(roleService.findById(id), RoleDto.class);
        return new ResponseEntity<>(responseRole, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<RoleDto> create(@Valid @RequestBody CreateOrUpdateRoleDto roleDto){
        Role role = mapper.convert(roleDto, Role.class);
        RoleDto responseRole = mapper.convert(roleService.createRole(role), RoleDto.class);
        return new ResponseEntity<>(responseRole, HttpStatus.CREATED);
    }

    @PutMapping("{id}")
    public ResponseEntity<RoleDto> update(
            @PathVariable Long id,
            @RequestBody CreateOrUpdateRoleDto roleDto) {

        Role role = mapper.convert(roleDto, Role.class);
        RoleDto responseRole = mapper.convert(roleService.updateRole(id, role), RoleDto.class);
        return new ResponseEntity<>(responseRole, HttpStatus.OK);
    }

    @PatchMapping("{id}")
    public ResponseEntity<RoleDto> patch(
            @PathVariable Long id,
            @Valid @RequestBody CreateOrUpdateRoleDto roleDto) {

        Role role = mapper.convert(roleDto, Role.class);
        Role patchedRole = roleService.patchRole(id, role);
        return new ResponseEntity<>(mapper.convert(patchedRole, RoleDto.class), HttpStatus.OK);
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable Long id){
        roleService.deleteRoleById(id);
    }
}
