package com.builovn.webapp.controller;

import com.builovn.webapp.domain.dto.department.CreateOrUpdateDepartmentDto;
import com.builovn.webapp.domain.dto.department.DepartmentDto;
import com.builovn.webapp.domain.entity.Department;
import com.builovn.webapp.domain.entity.Speciality;
import com.builovn.webapp.domain.mapper.MapperDispatcher;
import com.builovn.webapp.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/v1/departments")
public class DepartmentController extends BaseController{

    private final DepartmentService departmentService;
    private final MapperDispatcher mapper;

    public DepartmentController(DepartmentService departmentService, MapperDispatcher mapper) {
        this.departmentService = departmentService;
        this.mapper = mapper;
    }

    @GetMapping
    public ResponseEntity<List<DepartmentDto>> list() {
        List<DepartmentDto> departmentList = mapper.convertList(departmentService.findAll(), DepartmentDto.class);
        return new ResponseEntity<>(departmentList, HttpStatus.OK);
    }

    @GetMapping("{id}")
    public ResponseEntity<DepartmentDto> getOne(@PathVariable Long id){
        DepartmentDto responseDepartment = mapper.convert(departmentService.findById(id), DepartmentDto.class);
        return new ResponseEntity<>(responseDepartment, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<DepartmentDto> create(@Valid @RequestBody CreateOrUpdateDepartmentDto departmentDto){
        Department department = mapper.convert(departmentDto, Department.class);
        DepartmentDto responseDepartment = mapper.convert(departmentService.createDepartment(department),
                DepartmentDto.class);
        return new ResponseEntity<>(responseDepartment, HttpStatus.CREATED);
    }

    @PutMapping("{id}")
    public ResponseEntity<DepartmentDto> update(
            @PathVariable Long id,
            @Valid @RequestBody CreateOrUpdateDepartmentDto departmentDto) {

        Department department = mapper.convert(departmentDto, Department.class);
        DepartmentDto responseDepartment = mapper.convert(departmentService.updateDepartment(id, department),
                DepartmentDto.class);
        return new ResponseEntity<>(responseDepartment, HttpStatus.OK);
    }

    @PatchMapping("{id}")
    public ResponseEntity<DepartmentDto> patch(
            @PathVariable Long id,
            @RequestBody CreateOrUpdateDepartmentDto departmentDto) {

        Department department = mapper.convert(departmentDto, Department.class);
        Department patchedDepartment = departmentService.patchDepartment(id, department);
        return new ResponseEntity<>(mapper.convert(patchedDepartment, DepartmentDto.class), HttpStatus.OK);
    }

    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable Long id){
        departmentService.deleteDepartmentById(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
