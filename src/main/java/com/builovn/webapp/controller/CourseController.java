package com.builovn.webapp.controller;

import com.builovn.webapp.domain.dto.course.CourseDto;
import com.builovn.webapp.domain.entity.Course;
import com.builovn.webapp.domain.mapper.MapperDispatcher;
import com.builovn.webapp.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/v1/courses")
public class CourseController extends BaseController{
    private final CourseService courseService;
    private final MapperDispatcher mapper;

    @Autowired
    public CourseController(
            CourseService courseService,
            MapperDispatcher mapper
    ){
        this.courseService = courseService;
        this.mapper = mapper;
    }

    @GetMapping
    public ResponseEntity<List<CourseDto>> list() {
        List<CourseDto> courseList = mapper.convertList(courseService.findAll(), CourseDto.class);
        return new ResponseEntity<>(courseList, HttpStatus.OK);
    }

    @GetMapping("{value}")
    public ResponseEntity<CourseDto> getOne(@PathVariable Byte value){
        CourseDto responseCourse = mapper.convert(courseService.findById(value), CourseDto.class);
        return new ResponseEntity<>(responseCourse, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<CourseDto> create(@Valid @RequestBody CourseDto courseDto){
        Course course = mapper.convert(courseDto, Course.class);
        CourseDto responseCourse = mapper.convert(courseService.createCourse(course), CourseDto.class);
        return new ResponseEntity<>(responseCourse, HttpStatus.CREATED);
    }

    @DeleteMapping("{value}")
    public ResponseEntity delete(@PathVariable Byte value){
        courseService.deleteCourseById(value);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
