package com.builovn.webapp.controller;

import com.builovn.webapp.domain.dto.composite.CourseSpecialityDto;
import com.builovn.webapp.domain.dto.composite.RoleDepartmentDto;
import com.builovn.webapp.domain.dto.group.GroupDto;
import com.builovn.webapp.domain.dto.keycloak.CreateKeycloakUserDto;
import com.builovn.webapp.domain.dto.user.*;
import com.builovn.webapp.domain.entity.*;
import com.builovn.webapp.domain.mapper.MapperDispatcher;
import com.builovn.webapp.service.KeycloakService;
import com.builovn.webapp.service.UserService;
import org.keycloak.KeycloakSecurityContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/v1/users")
public class UserController extends BaseController{

    private final UserService userService;
    private final MapperDispatcher mapper;
    private final KeycloakService keycloakService;

    @Autowired
    public UserController(
            UserService userService,
            MapperDispatcher mapper,
            KeycloakService keycloakService
    ) {
        this.userService = userService;
        this.mapper = mapper;
        this.keycloakService = keycloakService;
    }

    @GetMapping
    public ResponseEntity<List<UserDto>> list() {
        List<UserDto> userList = mapper.convertList(userService.findAll(), UserDto.class);
        return new ResponseEntity<>(userList, HttpStatus.OK);
    }

    @GetMapping("{id}")
    public ResponseEntity<UserDto> getOneById(@PathVariable String id){
        UserDto responseUser = mapper.convert(userService.findById(id), UserDto.class);
        return new ResponseEntity<>(responseUser, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<UserDto> create(@Valid @RequestBody CreateOrUpdateUserDto userDto){
        CreateKeycloakUserDto keycloakUser = mapper.convert(userDto, CreateKeycloakUserDto.class);
        String userId = keycloakService.createKeycloakUser(keycloakUser, getToken());
        userDto.setId(userId);
        User user = mapper.convert(userDto, User.class);
        UserDto responseUser = mapper.convert(userService.createUser(user), UserDto.class);
        return new ResponseEntity<>(responseUser, HttpStatus.CREATED);
    }

    @PutMapping("{id}")
    public ResponseEntity<UserDto> update(
            @PathVariable String id,
            @RequestBody CreateOrUpdateUserDto userDto) {

        User user = mapper.convert(userDto, User.class);
        UserDto responseUser = mapper.convert(userService.updateUser(id, user), UserDto.class);
        return new ResponseEntity<>(responseUser, HttpStatus.OK);
    }

    @PatchMapping("{id}")
    public ResponseEntity<UserDto> patch(
            @PathVariable String id,
            @Valid @RequestBody PatchUserDto patchUserDto){

        patchUserDto.getRoleDepartmentList().forEach(element -> element.setUserId(id));
        patchUserDto.getCourseSpecialityList().forEach(element -> element.setUserId(id));
        User user = mapper.convert(patchUserDto, User.class);
        User patchedUser = userService.patchUser(id, user);

        if(patchUserDto.getRoleDepartmentList() != null){
            List<String> roleDepartmentPairList = patchedUser.getUserRoleDepartments()
                    .stream()
                    .map(element -> element.getRole().getId() + ":" + element.getDepartment().getId())
                    .collect(Collectors.toList());

            keycloakService.setUserCustomAttribute(id, getToken(), "role-department",
                    roleDepartmentPairList);
        }
        return new ResponseEntity<>(mapper.convert(patchedUser, UserDto.class), HttpStatus.OK);

    }

    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable String id){
        userService.deleteUserById(id);
        keycloakService.deleteKeycloakUser(id, getToken());
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("{id}/groups")
    public ResponseEntity<List<GroupDto>> getGroups(@PathVariable String id){
        List<GroupDto> responseDto = mapper.convertList(userService.findAllUserGroupByUserId(id), GroupDto.class);
        return new ResponseEntity<>(responseDto, HttpStatus.OK);
    }

    @PostMapping("{id}/groups")
    public ResponseEntity<GroupDto> addGroup(
            @PathVariable String id,
            @Valid @RequestBody AddGroupDto addGroupDto){

        Group group = mapper.convert(addGroupDto, Group.class);
        GroupDto responseDto = mapper.convert(userService.addGroup(id, group), GroupDto.class);
        return new ResponseEntity<>(responseDto, HttpStatus.CREATED);
    }

    @DeleteMapping("{id}/groups/{groupId}")
    public ResponseEntity<AddGroupDto> deleteGroup(
            @PathVariable String id,
            @PathVariable Long groupId){
        userService.deleteGroup(id, groupId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
