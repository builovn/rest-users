package com.builovn.webapp.controller;

import com.builovn.webapp.domain.dto.role.RoleDto;
import com.builovn.webapp.domain.dto.speciality.CreateOrUpdateSpecialityDto;
import com.builovn.webapp.domain.dto.speciality.SpecialityDto;
import com.builovn.webapp.domain.entity.Department;
import com.builovn.webapp.domain.entity.Role;
import com.builovn.webapp.domain.entity.Speciality;
import com.builovn.webapp.domain.mapper.MapperDispatcher;
import com.builovn.webapp.service.DepartmentService;
import com.builovn.webapp.service.SpecialityService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/v1/departments/{departmentId}/specialities")
public class SpecialityController extends BaseController{

    private final SpecialityService specialityService;
    private final DepartmentService departmentService;
    private final MapperDispatcher mapper;

    public SpecialityController(
            SpecialityService specialityService,
            MapperDispatcher mapper,
            DepartmentService departmentService) {

        this.specialityService = specialityService;
        this.departmentService =departmentService;
        this.mapper = mapper;
    }

    @GetMapping
    public ResponseEntity<List<SpecialityDto>> list(@PathVariable Long departmentId) {
        List<Speciality> specialities = specialityService.findAllByDepartmentId(departmentId);
        List<SpecialityDto> specialityList = mapper.convertList(specialities, SpecialityDto.class);
        return new ResponseEntity<>(specialityList, HttpStatus.OK);
    }

    @GetMapping("{id}")
    public ResponseEntity<SpecialityDto> getOne(
            @PathVariable Long id,
            @PathVariable Long departmentId){

        Speciality speciality = specialityService.findOneByIdAndDepartmentId(id, departmentId);
        SpecialityDto responseSpeciality = mapper.convert(speciality, SpecialityDto.class);
        return new ResponseEntity<>(responseSpeciality, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<SpecialityDto> create(
            @PathVariable Long departmentId,
            @Valid @RequestBody CreateOrUpdateSpecialityDto specialityDto){

        Department department = departmentService.findById(departmentId);
        Speciality speciality = mapper.convert(specialityDto, Speciality.class);
        speciality.setDepartment(department);
        SpecialityDto responseSpeciality = mapper.convert(specialityService.createSpeciality(speciality),
                SpecialityDto.class);
        return new ResponseEntity<>(responseSpeciality, HttpStatus.CREATED);
    }

    @PutMapping("{id}")
    public ResponseEntity<SpecialityDto> update(
            @PathVariable Long id,
            @PathVariable Long departmentId,
            @RequestBody CreateOrUpdateSpecialityDto specialityDto) {

        Speciality speciality = mapper.convert(specialityDto, Speciality.class);
        SpecialityDto responseSpeciality = mapper.convert(
                specialityService.updateSpecialityOfDepartment(id, speciality, departmentId), SpecialityDto.class);
        return new ResponseEntity<>(responseSpeciality, HttpStatus.OK);
    }

    @PatchMapping("{id}")
    public ResponseEntity<SpecialityDto> patch(
            @PathVariable Long id,
            @PathVariable Long departmentId,
            @RequestBody CreateOrUpdateSpecialityDto specialityDto){

        Speciality speciality = mapper.convert(specialityDto, Speciality.class);
        Speciality patchedSpeciality = specialityService.patchSpecialityOfDepartment(id, speciality, departmentId);
        return new ResponseEntity<>(mapper.convert(patchedSpeciality, SpecialityDto.class), HttpStatus.OK);
    }

    @DeleteMapping("{id}")
    public ResponseEntity delete(
            @PathVariable Long id,
            @PathVariable Long departmentId){

        specialityService.deleteSpecialityOfDepartmentById(id, departmentId);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
