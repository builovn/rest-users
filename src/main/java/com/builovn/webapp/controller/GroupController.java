package com.builovn.webapp.controller;

import com.builovn.webapp.domain.dto.group.CreateOrUpdateGroupDto;
import com.builovn.webapp.domain.dto.group.GroupDto;
import com.builovn.webapp.domain.dto.user.UserDto;
import com.builovn.webapp.domain.entity.Group;
import com.builovn.webapp.domain.entity.User;
import com.builovn.webapp.domain.mapper.MapperDispatcher;
import com.builovn.webapp.service.GroupService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.GroupDefinitionException;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/v1/groups")
public class GroupController extends BaseController{
    private final GroupService groupService;
    private final MapperDispatcher mapper;

    public GroupController(GroupService groupService, MapperDispatcher mapper) {
        this.groupService = groupService;
        this.mapper = mapper;
    }

    @GetMapping
    public ResponseEntity<List<GroupDto>> list() {
        List<GroupDto> groupList = mapper.convertList(groupService.findAll(), GroupDto.class);
        return new ResponseEntity<>(groupList, HttpStatus.OK);
    }

    @GetMapping("{id}")
    public ResponseEntity<GroupDto> getOne(@PathVariable Long id){
        GroupDto responseGroup = mapper.convert(groupService.findById(id), GroupDto.class);
        return new ResponseEntity<>(responseGroup, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<GroupDto> create(@Valid @RequestBody CreateOrUpdateGroupDto groupDto){
        Group group = mapper.convert(groupDto, Group.class);
        GroupDto responseGroup = mapper.convert(groupService.createGroup(group), GroupDto.class);
        return new ResponseEntity<>(responseGroup, HttpStatus.CREATED);
    }

    @PutMapping("{id}")
    public ResponseEntity<GroupDto> update(
            @PathVariable Long id,
            @Valid @RequestBody CreateOrUpdateGroupDto groupDto) {

        Group group = mapper.convert(groupDto, Group.class);
        GroupDto responseGroup = mapper.convert(groupService.updateGroup(id, group), GroupDto.class);
        return new ResponseEntity<>(responseGroup, HttpStatus.OK);
    }

    @PatchMapping("{id}")
    public ResponseEntity<GroupDto> patch(
            @PathVariable Long id,
            @Valid @RequestBody CreateOrUpdateGroupDto groupDto) {

        Group group = mapper.convert(groupDto, Group.class);
        Group patchedGroup = groupService.patchGroup(id, group);
        return new ResponseEntity<>(mapper.convert(patchedGroup, GroupDto.class), HttpStatus.OK);
    }

    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable Long id){
        groupService.deleteGroupById(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @GetMapping("{id}/users")
    public ResponseEntity<List<UserDto>> getGroups(@PathVariable Long id){
        List<UserDto> responseDto = mapper.convertList(groupService.findAllUserGroupByGroupId(id), UserDto.class);
        return new ResponseEntity<>(responseDto, HttpStatus.OK);
    }
}
