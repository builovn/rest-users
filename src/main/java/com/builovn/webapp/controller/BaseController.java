package com.builovn.webapp.controller;

import org.keycloak.KeycloakSecurityContext;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public abstract class BaseController {
    private HttpServletRequest request;

    @Autowired
    protected void setRequest(HttpServletRequest request){
        this.request = request;
    }

    protected HttpServletRequest getRequest(){
        return this.request;
    }

    protected String getToken()
    {
        String contextName = KeycloakSecurityContext.class.getName();
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(contextName);
        return context.getTokenString();
    }

    protected Map<String, Object> getTokenUserAttributes(){
        String contextName = KeycloakSecurityContext.class.getName();
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(contextName);
        return context.getToken().getOtherClaims();
    }
}
